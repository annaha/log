
var video;
var canvas


function setup() {
  c = createCanvas(window.innerWidth, window.innerHeight);
  background(51);
  video = createCapture(VIDEO);
  video.size(window.innerWidth, window.innerHeight);
  video.hide();
}



function draw() {
  // tint(255, 0, 0);
  push()
  translate(video.width, 0)
  scale(-1.0, 1.0);  
  image(video, mouseY, mouseX, mouseY, mouseX);
  image(video, mouseX, mouseY, mouseX, mouseY);
  image(video, mouseX, mouseX, mouseY, mouseY);
  image(video, mouseY, mouseY, mouseX, mouseX);
  pop()
}

 function keyPressed(){
  if(keyCode === 32){
    saveCanvas(canvas, 'intercam', '.png')
  } console.log(keyCode)
 }
